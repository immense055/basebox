<!--
SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# F-Droid Basebox

__This project is no longer maintained, we use the [official Debian boxes](https://app.vagrantup.com/debian) now.__

This script bootstraps Debian vagrant boxes which are a suitable base for
building F-Droid Build Server boxes. It supports 'libvirt' (qemu/kvm) and
'virtualbox'.

You can find the full documentation on F-Droid Build Server here:  
https://f-droid.org/docs/Build_Server_Setup

## Pre-built boxes

For convenience we distribute pre-built vagrant boxes on:
https://app.vagrantup.com/fdroid.

You can also download the latest Basebox images build by GitLab-CI.

<table>
<tr> <th></th> <th>libvirt</th> <th>virtualbox</th> </tr>

<tr> <th>Buster 🐕</th>
<td>

[Download](https://gitlab.com/fdroid/basebox/-/jobs/artifacts/master/raw/basebox-buster64-libvirt.box?job=build_buster_libvirt)

</td>
<td>

[Download](https://gitlab.com/fdroid/basebox/-/jobs/artifacts/master/raw/basebox-buster64-virtualbox.box?job=build_buster_virtualbox)

</td>
</tr>

<tr> <th>Stretch 🐙</th> 
<td>

[Download](https://gitlab.com/fdroid/basebox/-/jobs/artifacts/master/raw/basebox-stretch64-libvirt.box?job=build_stretch_libvirt)

</td>
<td>

[Download](https://gitlab.com/fdroid/basebox/-/jobs/artifacts/master/raw/basebox-stretch64-virtualbox.box?job=build_stretch_virtualbox)

</td>
</tr>
</table>

## Usage

### Example: bootstrap a libvirt box

This section describes the primary intended use case for
`fdroid_basebox`.

#### Fetch this script via git:

```shell
git clone https://gitlab.com/fdroid/basebox.git
cd basebox
```

Clone this project and cd to the project folder.

#### Bootstrap and add box:

```shell
sudo ./fdroid_basebox.py --provider libvirt
vagrant box add basebox-stretch64 basebox-stretch64-libvirt.box
```

Build a new basebox from scratch and add it to vagrant box storage.

#### (optional) Cleanup:

```shell
rm basebox-stretch64-libvirt.box
```

#### Configure makebuildserver to use this box

Set `basebox = basebox-stretch64` in your _makebuildserver.config.py_.

(Typically _makebuildserver.config.py_ is located in your checkout of
fdroidserver.)



### Synopsis

    fdroid_basebox.py [-h] [-v] [-q] [-t TARGET] [-p PROVIDER]
                      [-d] [--workdir WORKDIR]

### Options

    -h, --help            show this help message and exit
    -v, --verbose         Spew out even more information than normal
    -q, --quiet           Restrict output to warnings and errors
    -t TARGET, --target TARGET
                          target debian version. defaults to 'stretch64'.
                          (supported: stretch64, buster64)
    -p PROVIDER, --provider PROVIDER
                          target vagrant provider. defaults to 'virtualbox'.
                          (supported: libvirt, virtualbox)
    -d, --dry-run         don't actually bulild the image
    --workdir WORKDIR     put intermediate steps into requested path, instead
                          of temporary dir

## FAQ

### Why does this script need root?

Because it's using `vmdebootstrap` for building the box. Typically,
_debootstrap_ is only available for super users, because it's
bootstrapping an entirely new Debian system inside a change-root.
So root privileges are needed for assigning file-access permissions.
_vmdebootstrap_ additionally needs to mount a loop-back device for
dumping the change-root as a raw disk image.
